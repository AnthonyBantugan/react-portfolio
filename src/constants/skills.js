import HTML from "../assets/img/logo-html5.png";
import CSS from "../assets/img/logo-css3.png";
import JS from "../assets/img/logo-javascript.png";
import ReactImg from "../assets/img/logo-react.png";
import MongoDB from "../assets/img/logo-mongodb.png";
import Bootstrap from "../assets/img/logo-bootstrap.png";
import Express from "../assets/img/logo-expressjs.png";
import Git from "../assets/img/logo-git.png";
import Node from "../assets/img/logo-nodejs.png";
import Tailwind from "../assets/img/logo-tailwind.png";
import Typescript from "../assets/img/ts.png";
import ReactNative from "../assets/img/react-native.png";
import Next from "../assets/img/next.png";
import Gatsby from "../assets/img/gatsby.png";
import Expo from "../assets/img/expo.png";

export const SKILLS = [
  {
    name: "html",
    src: HTML,
  },
  {
    name: "css",
    src: CSS,
  },
  {
    name: "javascript",
    src: JS,
  },
  {
    name: "typescript",
    src: Typescript,
  },
  {
    name: "bootstrap",
    src: Bootstrap,
  },
  {
    name: "tailwind css",
    src: Tailwind,
  },
  {
    name: "react js",
    src: ReactImg,
  },
  {
    name: "react native",
    src: ReactNative,
  },
  {
    name: "next js",
    src: Next,
  },
  {
    name: "gatsby js",
    src: Gatsby,
  },
  {
    name: "node js",
    src: Node,
  },
  {
    name: "express",
    src: Express,
  },
  {
    name: "mongo db",
    src: MongoDB,
  },
  {
    name: "expo",
    src: Expo,
  },
  {
    name: "git",
    src: Git,
  },
];
