import { FaLinkedin, FaGithub, FaGitlab, FaWhatsapp } from "react-icons/fa";
import { BsFillPersonLinesFill } from "react-icons/bs";
import Resume from "../assets/pdf/Resume-Anthony-Karlo-Bantugan.pdf";

export const NAV_ITEMS = [
  {
    name: "About",
    link: "about",
  },
  {
    name: "Skills",
    link: "skills",
  },
  {
    name: "Projects",
    link: "projects",
  },
  {
    name: "Contact",
    link: "contact",
  },
];

export const SOCIALS = [
  {
    text: "LinkedIn",
    name: "linkedin",
    bg: "bg-[rgb(10,102,194)]",
    link: "https://www.linkedin.com/in/anthony-karlo-bantugan-3059b0227/",
    icon: <FaLinkedin size={30} />,
  },
  {
    text: "GitHub",
    name: "github",
    bg: "bg-[rgb(23,21,21)]",
    link: "https://github.com/AnthonyKarloBantugan",
    icon: <FaGithub size={30} />,
  },
  {
    text: "GitLab",
    name: "gitlab",
    bg: "bg-[#D82F16]",
    link: "https://gitlab.com/AnthonyBantugan",
    icon: <FaGitlab size={30} />,
  },
  {
    text: "+63 998 569 1758",
    name: "phone",
    bg: "bg-[#4CCA5A]",
    link: "tel:+639985691758",
    icon: <FaWhatsapp size={30} />,
  },
  {
    text: "Resume",
    name: "resume",
    bg: "bg-[#565f69]",
    link: Resume,
    icon: <BsFillPersonLinesFill size={30} />,
    dowload: "Anthony Karlo Bantugan Resume",
  },
];
