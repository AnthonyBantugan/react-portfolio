import Randomzr from "../assets/img/randomzr.jpg";
import Onpoint from "../assets/img/onpoint.jpg";
import Agfirst from "../assets/img/agfirst.jpg";
import Techlabs from "../assets/img/techlabs.jpg";

export const PROJECTS = [
  {
    name: "Randomzr App",
    desc: " Randomzr is a free platform that allows you to randomize anything.",
    link: "https://randomzr.com/",
    img: `url(${Randomzr})`,
  },
  {
    name: "OnPoint Design Website",
    desc: "A portfolio website for an architecture and design company",
    link: "https://onpointdreamworks.com/",
    img: `url(${Onpoint})`,
  },
  {
    name: "Livestock Barometer Web and Mobile App",
    desc: "An app that delivers real-time updates, offering a snapshot of the marketplace for your selected stock classes",
    link: "https://www.livestockbarometer.com/",
    img: `url(${Agfirst})`,
  },
  {
    name: "TechLabs Ecommerce",
    desc: "A personal Ecommerce site for computer hardware",
    link: "https://capstone-3-mu.vercel.app/",
    img: `url(${Techlabs})`,
  },
];
