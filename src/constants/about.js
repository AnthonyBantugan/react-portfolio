export const EDUCATION = [
  {
    year: "2021 - 2022",
    school: "Zuitt Coding Bootcamp",
    course: "Certificate of Completion",
  },
  {
    year: "2008 - 2012",
    school: "Ateneo de Davao University",
    course: "BS- Management Accounting",
  },
];

export const EXPERIENCE = [
  {
    year: "2022 - present",
    company: "Mugna Technologies Inc.",
    role: "Software Engineer",
  },
  {
    year: "2020 - 2021",
    company: "Upwork",
    role: " Freelance Web Developer",
  },
];
