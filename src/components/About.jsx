import React, { Fragment } from "react";
import { EDUCATION, EXPERIENCE } from "../constants/about";

const About = () => {
  const Heading = ({ children }) => (
    <div className="sm:text-right pb-8 pl-4">
      <p className="text-4xl font-bold inline border-b-4 border-pink-600">
        {children}
      </p>
    </div>
  );

  const Intro = () => (
    <div className="max-w-[1000px] w-full grid sm:grid-row-2 gap-8 px-4">
      <div className=" text-4xl font-bold">
        <p>Hi! I'm Anthony.</p>
      </div>
      <div className="flex flex-col gap-4">
        <p className="tracking-wider">
          In this portfolio, you’ll not only see technical proficiency but also
          creativity and adaptability in each project. From dynamic web
          applications to streamlined backend systems, each one represents
          problem-solving in a world that never stops changing.
        </p>
        <p className="tracking-wider">
          Beyond the code, I love working as part of a team. It allows ideas to
          flow freely while collective wisdom propels projects forward.
        </p>
      </div>
    </div>
  );

  const Education = () => (
    <>
      <div className="max-w-[1000px] w-full grid grid-cols-2 gap-8 mt-8">
        <Heading>Education</Heading>
      </div>
      <div className="max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 px-4">
        {EDUCATION?.map(({ year, school, course }, i) => (
          <Fragment key={i}>
            <div className="sm:text-right text-3xl font-bold">
              <p>{year}</p>
            </div>
            <div>
              <p className=" text-3xl tracking-wider">{school}</p>
              {course}
            </div>
          </Fragment>
        ))}
      </div>
    </>
  );

  const Experience = () => (
    <>
      <div className="max-w-[1000px] w-full grid grid-cols-2 gap-8 mt-8">
        <Heading>Professional Experience</Heading>
      </div>
      {EXPERIENCE?.map(({ year, company, role }, i) => (
        <div
          key={i}
          className="max-w-[1000px] w-full grid sm:grid-cols-2 gap-8 p-2"
        >
          <div className="sm:text-right text-3xl font-bold ">
            <p>{year}</p>
          </div>
          <div>
            <p className=" text-3xl tracking-wider">{company}</p>
            {role}
          </div>
        </div>
      ))}
    </>
  );

  return (
    <div
      name="about"
      className="w-full sm:min-h-screen bg-[#0a192f] text-gray-300"
    >
      <div className="flex flex-col  justify-center items-center w-full h-full">
        <div className="max-w-[1000px] w-full grid grid-cols-2 gap-8">
          <Heading>About</Heading>
        </div>
        <Intro />
        <Experience />
        <Education />
      </div>
    </div>
  );
};

export default About;
