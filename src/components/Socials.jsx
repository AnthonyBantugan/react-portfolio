import React from "react";
import { SOCIALS } from "../constants/hero";

const Socials = () => {
  return (
    <div className="hidden lg:flex fixed flex-col top-[35%] left-0">
      <ul>
        {SOCIALS?.map(({ name, bg, link, icon, dowload, text }) => (
          <li
            key={name}
            className={`${
              name === "phone" ? "w-[205px]" : "w-[160px]"
            } h-[60px] flex justify-between items-center ${
              name === "phone" ? "ml-[-145px]" : "ml-[-100px]"
            } hover:ml-[-10px] duration-300 ${bg}`}
          >
            <a
              className="flex justify-between items-center w-full text-white"
              href={link}
              target="_blank"
              rel="noreferrer"
              download={dowload}
            >
              {text} {icon}
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Socials;
