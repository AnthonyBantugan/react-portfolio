import React, { useState } from "react";
import { FaBars, FaTimes } from "react-icons/fa";
import Logo from "../assets/img/logo.png";
import { Link } from "react-scroll";
import { NAV_ITEMS } from "../constants/hero";
import Socials from "./Socials";

const Navbar = () => {
  const [nav, setNav] = useState(false);
  const handleClick = () => setNav(!nav);

  return (
    <div className="fixed w-full h-[80px] flex justify-between items-center px-4 bg-[#0a192f] text-gray-300">
      <div>
        <Link to="home" smooth={true} duration={500}>
          <img
            src={Logo}
            alt="logo"
            style={{ width: "150px", cursor: "pointer" }}
          />
        </Link>
      </div>

      {/* Nav-Menu */}
      <ul className="hidden md:flex">
        {NAV_ITEMS?.map(({ name, link }) => (
          <li key={name}>
            <Link to={link} smooth={true} duration={500}>
              {name}
            </Link>
          </li>
        ))}
      </ul>

      {/* Hamburger Icon and Close Icon */}
      <div onClick={handleClick} className="md:hidden z-10">
        {!nav ? <FaBars /> : <FaTimes />}
      </div>

      {/* Mobile Menu */}
      <ul
        className={
          !nav
            ? "hidden"
            : "absolute top-0 left-0 w-full h-screen bg-[#0a192f] flex flex-col justify-center items-center"
        }
      >
        {NAV_ITEMS?.map(({ name, link }) => (
          <li key={name} className="py-6 text-4xl">
            <Link onClick={handleClick} to={link} smooth={true} duration={500}>
              {name}
            </Link>
          </li>
        ))}
      </ul>
      <Socials />
    </div>
  );
};

export default Navbar;
